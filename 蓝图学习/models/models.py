from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import sessionmaker


engine = create_engine('mysql+pymysql://root:python@localhost:3306/python_flask?charset=utf8')
Base = declarative_base()


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)
    user_id = Column(String(50), nullable=False)
    user_name = Column(String(50), nullable=False)
    head_img = Column(String(200))
    short_description = Column(String(300))
    pass_word = Column(String(100), nullable=False)
