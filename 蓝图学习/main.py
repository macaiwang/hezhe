from flask import Flask
from 蓝图学习.views.admin import admin_blu
from 蓝图学习.views.index import index_blu


# 创建应用对象
# print("创建应用对象")
app = Flask(__name__)

# 将蓝图注册到app上
# print("将蓝图注册到app上")
app.register_blueprint(admin_blu, url_prefix="/admin")
app.register_blueprint(index_blu)


if __name__ == '__main__':
    print("main函数使用")
    app.run(port="4848")
    # print(app.url_map)
