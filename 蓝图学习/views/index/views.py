from flask import render_template, request, url_for, redirect, make_response
from sqlalchemy.orm import sessionmaker

from models.models import engine, User
from . import index_blu


LOGINFLAG = False


@index_blu.route("/profile2/<int:a>")
def profile1(a):
    print(request.cookies)
    login_flag = request.cookies.get("login_flag")
    if not login_flag:
        return redirect(url_for(".login"))


@index_blu.route("/login")
def login():
    if request.args:
        user_name = request.args.get("Username")
        pass_word = request.args.get("Password")
        session = sessionmaker(bind=engine)()
        user_ret = session.query(User).filter(User.user_name == user_name, User.pass_word == pass_word).first()
        session.close()
        if user_ret:
            response = make_response("登录成功")
            response.set_cookie("login_flag", "success")
            response.set_cookie("user_id", user_ret.user_id)
            return response
        else:
            return "登录失败"
    else:
        return render_template("index/login.html")


