from flask import render_template
from sqlalchemy.orm import sessionmaker

from models.models import engine, User
from . import admin_blu


@admin_blu.route("/index")
def index():
    session = sessionmaker(bind=engine)()
    users_info = session.query(User).all()
    print(users_info)
    session.close()
    return render_template("admin/index2.html", users=users_info )
    # return render_template("index.html", users=users_info)
